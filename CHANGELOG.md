# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.6.1

- patch: Internal maintenance: Add required tags for test infra resources.

## 1.6.0

- minor: Bump pipes-toolkit -> 2.2.0.

## 1.5.0

- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 1.4.0

- minor: Make TASK_DEFINITION variable optional. It could be useful when FORCE_NEW_DEPLOYMENT variable used. Use this option if you need to trigger a new deployment with no service definition changes: if your updated Docker image uses the same tag as what is in the existing task definition for your service (for example, my_image:latest).

## 1.3.0

- minor: Add support for the FORCE_NEW_DEPLOYMENT variable. Use this option if you need to trigger a new deployment with no service definition changes: if your updated Docker image uses the same tag as what is in the existing task definition for your service (for example, my_image:latest).

## 1.2.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 1.2.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 1.1.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 1.1.3

- patch: Internal maintenance: Update pipe's depencies.

## 1.1.2

- patch: Internal maintenance: Add gitignore secrets.

## 1.1.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 1.1.0

- minor: Add default values for AWS variables.

## 1.0.7

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 1.0.6

- patch: Add warning message about new version of the pipe available.

## 1.0.5

- patch: Internal maintenance: Add auto infrastructure for tests.

## 1.0.4

- patch: Fix readme example.

## 1.0.3

- patch: Internal release

## 1.0.2

- patch: Added code style checks

## 1.0.1

- patch: Fixed log's messages

## 1.0.0

- major: Parameter names were simplified

## 0.3.0

- minor: Fix the bug when one of the parameters was validated as required while it was actually optional

## 0.2.2

- patch: Fixed the bug with missing pipe.yml

## 0.2.1

- patch: Fixed the docker image

## 0.2.0

- minor: Changed pipe to use the full task definition json instead of only using the container definitions

## 0.1.0

- minor: Initial release

