import configparser
from contextlib import contextmanager
from copy import copy
import json
import importlib
import io
import os
import shutil
import sys
from unittest import TestCase, mock

import yaml
import pytest
import botocore.session
from botocore.stub import Stubber, ANY

from pipe import main
from test.test import task_definition


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class SuccessTest(TestCase):

    @classmethod
    def setUpClass(cls):
        # update task_definition
        td = json.loads(task_definition)
        td['family'] = 'aaa'
        td['containerDefinitions'][0]['name'] = 'aaa'

        with open('test/taskDefinition.json', 'w') as outfile:
            json.dump(td, outfile)

    @classmethod
    def tearDownClass(cls):
        super().tearDownClass()
        os.remove('test/taskDefinition.json')

    @mock.patch.dict(os.environ, {'CLUSTER_NAME': 'fake-cluster',
                                  'SERVICE_NAME': 'fake-service',
                                  'TASK_DEFINITION': 'test/taskDefinition.json',
                                  'WAIT': 'True',
                                  'AWS_ACCESS_KEY_ID': 'akiafake',
                                  'AWS_SECRET_ACCESS_KEY': 'fakesecretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'BITBUCKET_COMMIT': 'abecdf',
                                  'BITBUCKET_BUILD_NUMBER': '1',
                                  'BITBUCKET_REPO_FULL_NAME': 'fake/fake', 'AWS_OIDC_ROLE_ARN': ''})
    @mock.patch.object(main.ECSDeploy, 'get_client')
    def test_aws_ecs_deploy(self, mock_ecs):
        ecs = botocore.session.get_session().create_client('ecs')
        mock_ecs.return_value = ecs

        with open('test/taskDefinition.json', 'r') as td:
            task_definition = json.load(td)

        stubber = Stubber(ecs)
        stubber.add_response(
            'register_task_definition',
            {'taskDefinition': {'taskDefinitionArn': 'aaa'}},
            expected_params=task_definition
        )
        stubber.add_response(
            'update_service',
            {
                'service': {
                    'serviceName': 'fake-service'
                }
            },
            expected_params={
                'cluster': 'fake-cluster',
                'service': 'fake-service',
                'taskDefinition': ANY,
                'forceNewDeployment': False,
            }
        )
        stubber.add_response(
            'describe_services',
            {
                'services': [
                    {
                        'serviceName': 'fake-service',
                        'deployments': [
                            {'taskDefinition': 'aaa'}
                        ]
                    }
                ],
                'failures': []
            },
            expected_params={
                'cluster': ANY,
                'services': ANY
            }
        )

        stubber.activate()
        current_pipe_module = importlib.import_module('pipe.main')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        ecs_deploy_pipe = current_pipe_module.ECSDeploy(
            schema=current_pipe_module.schema,
            pipe_metadata=metadata,
            check_for_newer_version=True
        )
        with capture_output() as out:
            ecs_deploy_pipe.run()

        self.assertRegex(
            out.getvalue(),
            rf"✔ Successfully updated the {os.getenv('SERVICE_NAME')} service"
        )


class NegativeTest(TestCase):
    @mock.patch.object(main.ECSDeploy, 'update_task_definition', return_value='aaa')
    @mock.patch.object(main.ECSDeploy, 'list_task_definitions', return_value=[])
    @mock.patch.object(main.ECSDeploy, 'update_service', return_value='response')
    @mock.patch.dict(os.environ, {'CLUSTER_NAME': 'fake-cluster',
                                  'SERVICE_NAME': 'fake-service',
                                  'WAIT': 'True',
                                  'AWS_ACCESS_KEY_ID': 'akiafake',
                                  'AWS_SECRET_ACCESS_KEY': 'fakesecretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'BITBUCKET_COMMIT': 'abecdf',
                                  'BITBUCKET_BUILD_NUMBER': '1',
                                  'BITBUCKET_REPO_FULL_NAME': 'fake/fake', 'AWS_OIDC_ROLE_ARN': ''})
    def test_aws_ecs_deploy(self, mock_ecs, _, mock_a):

        current_pipe_module = importlib.import_module('pipe.main')

        with open(os.path.join(os.getcwd(), 'pipe.yml')) as f:
            metadata = yaml.safe_load(f.read())
        ecs_deploy_pipe = current_pipe_module.ECSDeploy(
            schema=current_pipe_module.schema,
            pipe_metadata=metadata,
            check_for_newer_version=True
        )
        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                ecs_deploy_pipe.run()

        self.assertEquals(pytest_wrapped_e.type, SystemExit)
        self.assertRegex(
            out.getvalue(),
            rf"✖ There are no active tasks defifnitions for the service {os.getenv('SERVICE_NAME')} on the cluster {os.getenv('CLUSTER_NAME')}"
        )


class ECSDeployAuthTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    @classmethod
    def tearDownClass(cls):
        sys.path = cls.sys_path
        if os.path.exists(os.path.join(os.environ["HOME"], '.aws')):
            shutil.rmtree(os.path.join(os.environ["HOME"], '.aws'))

    @mock.patch('pipe.main.logger.info')
    @mock.patch.dict(os.environ, {'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'CLUSTER_NAME': 'test-cluster', 'SERVICE_NAME': 'test-service',
                                  'AWS_OIDC_ROLE_ARN': ''})
    def test_discover_auth_method_default_credentials_only(self, mock_logger):
        from pipe import main

        ecs_pipe = main.ECSDeploy(schema=main.schema, check_for_newer_version=True)
        mock_logger.assert_called_with('Using default authentication with '
                                       'AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.')
        ecs_pipe.auth()

        self.assertEqual(ecs_pipe.auth_method, 'DEFAULT_AUTH')
        self.assertFalse(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))

    @mock.patch('pipe.main.logger.info')
    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'CLUSTER_NAME': 'test-cluster', 'SERVICE_NAME': 'test-service'})
    def test_discover_auth_method_oidc_only(self, mock_logger):
        from pipe import main

        ecs_pipe = main.ECSDeploy(schema=main.schema, check_for_newer_version=True)
        ecs_pipe.auth()

        mock_logger.assert_called_with('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
        self.assertEqual(ecs_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))

        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

    @mock.patch('pipe.main.logger.info')
    @mock.patch.dict(os.environ, {'BITBUCKET_STEP_OIDC_TOKEN': 'token', 'AWS_OIDC_ROLE_ARN': 'account/role',
                                  'AWS_ACCESS_KEY_ID': 'akiafkae', 'AWS_SECRET_ACCESS_KEY': 'secretkey',
                                  'AWS_DEFAULT_REGION': 'test-region',
                                  'CLUSTER_NAME': 'test-cluster', 'SERVICE_NAME': 'test-service'})
    def test_discover_auth_method_oidc_and_default_credentials(self, mock_logger):
        from pipe import main
        self.assertTrue('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertTrue('AWS_SECRET_ACCESS_KEY' in os.environ)

        ecs_pipe = main.ECSDeploy(schema=main.schema, check_for_newer_version=True)
        ecs_pipe.auth()

        mock_logger.assert_called_with('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')

        self.assertEqual(ecs_pipe.auth_method, 'OIDC_AUTH')
        self.assertTrue(os.path.exists(f'{os.environ["HOME"]}/.aws/.aws-oidc'))
        config = configparser.ConfigParser()
        config.read(f'{os.environ["HOME"]}/.aws/config')
        self.assertEqual(config['default'].get('role_arn'), 'account/role')
        token_file = config['default'].get('web_identity_token_file')

        with open(token_file) as f:
            self.assertEqual(f.read(), 'token')

        self.assertFalse('AWS_ACCESS_KEY_ID' in os.environ)
        self.assertFalse('AWS_SECRET_ACCESS_KEY' in os.environ)
