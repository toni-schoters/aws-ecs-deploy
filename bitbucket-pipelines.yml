image:
  name: python:3.7


setup: &setup
  step:
    name: Setup testing resources
    script:
      - STACK_NAME_CLUSTER="bbci-test-ecr-cluster-${BITBUCKET_BUILD_NUMBER}"
      - pipe: atlassian/aws-cloudformation-deploy:0.6.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: "us-east-1"
          STACK_NAME: ${STACK_NAME_CLUSTER}
          TEMPLATE: "./test/CloudformationStackTemplate_ecs_cluster.yml"
          CAPABILITIES: ['CAPABILITY_IAM']
          WAIT: 'true'
          STACK_PARAMETERS: >
            [{
              "ParameterKey": "ClusterNameCustom",
              "ParameterValue": ${STACK_NAME_CLUSTER}
            }]
          TAGS: >
            [{
              "Key": "owner",
              "Value": "bbci-test-ecr-infrastructure"
            },
            {
              "Key": "Name",
              "Value": ${STACK_NAME_CLUSTER}
            },
            {
              "Key": "business_unit",
              "Value": "Engineering-Bitbucket"
            },
            {
              "Key": "service_name",
              "Value": "bitbucketci-pipes-aws-ecs-deploy"
            },
            {
              "Key": "resource_owner",
              "Value": "rgomis"
            }]

      - STACK_NAME_SERVICE="bbci-test-ecr-service-${BITBUCKET_BUILD_NUMBER}"
      - pipe: atlassian/aws-cloudformation-deploy:0.6.1
        variables:
          AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
          AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
          AWS_DEFAULT_REGION: "us-east-1"
          STACK_NAME: ${STACK_NAME_SERVICE}
          TEMPLATE: "./test/CloudformationStackTemplate_ecs_service.yml"
          CAPABILITIES: ['CAPABILITY_IAM']
          WAIT: 'true'
          STACK_PARAMETERS: >
            [
              {
                "ParameterKey": "StackName",
                "ParameterValue": ${STACK_NAME_CLUSTER}
              },
              {
                "ParameterKey": "ServiceName",
                "ParameterValue": ${STACK_NAME_SERVICE}
              }
            ]
          TAGS: >
              [{
                "Key": "owner",
                "Value": "bbci-test-ecr-infrastructure"
              }]


test: &test
  parallel:
    - step:
        name: Test
        oidc: true
        caches:
          - pip
        script:
        - pip install -r requirements.txt
        - pip install -r test/requirements.txt
        - pytest -p no:cacheprovider test/test_pipe_unit.py --capture=no --verbose
        - pytest test/test.py --verbose --capture=no
        - flake8 --ignore E501,E125
        after-script:
          - curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.2.9.zip" -o "awscliv2.zip" && unzip awscliv2.zip
          - echo 'c778f4cc55877833679fdd4ae9c94c07d0ac3794d0193da3f18cb14713af615f awscliv2.zip' | sha256sum -c - && ./aws/install
          - STACK_NAME_SERVICE="bbci-test-ecr-service-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME_SERVICE}
          - aws --region "us-east-1" cloudformation wait stack-delete-complete --stack-name ${STACK_NAME_SERVICE}
          - STACK_NAME_CLUSTER="bbci-test-ecr-cluster-${BITBUCKET_BUILD_NUMBER}"
          - aws --region "us-east-1" cloudformation delete-stack --stack-name ${STACK_NAME_CLUSTER}
        services:
        - docker
    - step:
        name: Lint the Dockerfile
        image: hadolint/hadolint:latest-debian
        script:
          - hadolint Dockerfile


releas-dev: &release-dev
  step:
    name: Release development version
    trigger: manual
    script:
      - set -ex
      - pip install semversioner
      - VERSION=$(semversioner current-version).${BITBUCKET_BUILD_NUMBER}-dev
      - pipe: atlassian/bitbucket-pipe-release:4.0.1
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
          GIT_PUSH: 'false'
          VERSION: ${VERSION}
    services:
      - docker


push: &push
  step:
    name: Push and Tag
    script:
      - pipe: atlassian/bitbucket-pipe-release:4.0.1
        variables:
          DOCKERHUB_USERNAME: $DOCKERHUB_USERNAME
          DOCKERHUB_PASSWORD: $DOCKERHUB_PASSWORD
          IMAGE: bitbucketpipelines/$BITBUCKET_REPO_SLUG
    services:
      - docker


pipelines:
  default:
  - <<: *setup
  - <<: *test
  - <<: *release-dev
  branches:
    master:
    - <<: *setup
    - <<: *test
    - <<: *push
