# Bitbucket Pipelines Pipe: AWS ECS deploy

Deploy a containerized application to AWS ECS

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-ecs-deploy:1.6.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    AWS_OIDC_ROLE_ARN: "<string>" # Optional by default. Required for OpenID Connect (OIDC) authentication.
    CLUSTER_NAME: '<string>'
    SERVICE_NAME: '<string>'
    TASK_DEFINITION: '<string>' # Optional
    FORCE_NEW_DEPLOYMENT: '<boolean>' # Optional
    # WAIT: '<boolean>' # Optional 
    # DEBUG: '<string>' # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**)              | AWS key id. |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret key. |
| AWS_DEFAULT_REGION (**) | AWS region. |
| AWS_OIDC_ROLE_ARN       | The ARN of the role used for web identity federation or OIDC. See **Authentication**. |
| CLUSTER_NAME (*) | The name of your ECS cluster. |
| SERVICE_NAME (*) | The name of your ECS service. |
| TASK_DEFINITION  | Path to the task definition json file. The file should contain a task definition as described in the [AWS docs][task definition parameters]. |
| FORCE_NEW_DEPLOYMENT | Whether to force a new deployment of the service. Default: `false`. Use this option if you need to trigger a new deployment with no service definition changes: if your updated Docker image uses the same tag as what is in the existing task definition for your service (for example, my_image:latest ). |
| WAIT | Wait until the service becomes stable. Default: `false`. The execution will fail if the service doensn't become stable and the timeout of 10 minutes is reached. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## Prerequisites

To use this pipe you should have an ECS cluster running a service. Learn how to create one [here][ECS cluster].

## Authentication

Supported options:

1. Environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Default option.

2. Assume role provider with OpenID Connect (OIDC). More details in the Bitbucket Pipelines Using OpenID Connect guide [Integrating aws bitbucket pipeline with oidc][aws-oidc]. Make sure that you setup OIDC before:
    * configure Bitbucket Pipelines as a Web Identity Provider in AWS
    * attach to provider your AWS role with required policies in AWS
    * setup a build step with `oidc: true` in your Bitbucket Pipelines
    * pass AWS_OIDC_ROLE_ARN (*) variable that represents role having appropriate permissions to execute actions on AWS ECS resources

## Examples

### Basic examples:

Deploy a new service configuration
```yaml
script:
  - pipe: atlassian/aws-ecs-deploy:1.6.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      CLUSTER_NAME: 'my-ecs-cluster'
      SERVICE_NAME: 'my-ecs-service'
      TASK_DEFINITION: 'task-definition.json'
```

Basic example. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: atlassian/aws-ecs-deploy:1.6.1
    variables:
      CLUSTER_NAME: 'my-ecs-cluster'
      SERVICE_NAME: 'my-ecs-service'
      TASK_DEFINITION: 'task-definition.json'
```

### Advanced examples:
Deploy a new service configuration with OpenID Connect (OIDC) alternative authentication without required `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`.
Parameter `oidc: true` in the step configuration and variable `AWS_OIDC_ROLE_ARN` are required:

```yaml

- step:
    oidc: true
    script:
      - pipe: atlassian/aws-ecs-deploy:1.6.1
        variables:
          AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
          AWS_OIDC_ROLE_ARN: 'arn:aws:iam::123456789012:role/role_name'
          CLUSTER_NAME: 'my-ecs-cluster'
          SERVICE_NAME: 'my-ecs-service'
          TASK_DEFINITION: 'task-definition.json'
```

If your updated Docker image uses the same tag as what is in the existing task definition for your service (for example, my_image:latest ), you do not need to create a new revision of your task definition. You can update the service using the FORCE_NEW_DEPLOYMENT option. The new tasks launched by the deployment pull the current image/tag combination from your repository when they start.

```yaml
script:
  - pipe: atlassian/aws-ecs-deploy:1.6.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: 'us-east-1'
      CLUSTER_NAME: 'my-ecs-cluster'
      SERVICE_NAME: 'my-ecs-service'
      FORCE_NEW_DEPLOYMENT: 'true'
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,aws,ecs
[ECS cluster]: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ECS_GetStarted_EC2.html
[aws-oidc]: https://support.atlassian.com/bitbucket-cloud/docs/deploy-on-aws-using-bitbucket-pipelines-openid-connect
[task definition parameters]: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task_definition_parameters.html
